# Test /add_answer

Questions:
- 2+2=? [**ID=0**]
  - 4
- 2+2=? [**ID=1**]
  - [ ] 1
  - [ ] 2
  - [ ] 3
  - [x] 4
- select first two [**ID=2**]
  - [x] 1
  - [x] 2
  - [ ] 3
  - [ ] 4
- do match [**ID=3**] 
  - categories: ```["<=3", ">3"]```
  - [ ] 1
  - [ ] 2
  - [ ] 3
  - [ ] 4

Response bodies:

0. text answer
```json
{
  "test_id": 1,
  "question_id": 0,
  "answer": {
    "text": "4"
  }
}
```
1. one option
```json
{
  "test_id": 1,
  "question_id": 1,
  "answer": {
    "options": [4]
  }
}
```

2. two options
```json
{
  "test_id": 1,
  "question_id": 2,
  "answer": {
    "options": [5, 6]
  }
}
```
3. categories
```json
{
  "test_id": 1,
  "question_id": 2,
  "answer": {
    "categories": {
      "<=3":  [1, 2, 3],
      ">3": [4]
    }
  }
}
```