import uuid
from collections import deque
from typing import List

from aiohttp import web

from server.app.auth.exceptions import FailedToCreateAPIKey, NoAPIKey, InvalidAPIKey
from server.db.init_db import Database, db
from server.db.models import APIUser


class AuthService:

    def __init__(self, db: Database):
        self.db = db
        self.active_users: deque[APIUser] = deque(maxlen=1000)

    def register_new(self) -> str:
        user = APIUser(key=uuid.uuid4().hex)
        try:
            self.db.store(user)
            self.active_users.appendleft(user)
        except:
            raise FailedToCreateAPIKey()

        return user.key

    def validate_key(self, key: str) -> bool:
        if self.get_user_by_key(key):
            return True
        return False

    def get_user_by_key(self, key: str) -> APIUser:
        if not key:
            raise NoAPIKey
        #
        # for user in self.active_users:
        #     if user.key == key:
        #         return user
        user = self.db.get_user(key)

        if not user:
            raise InvalidAPIKey
        return user




auth_service = AuthService(db)
