from server.utils.base import APIException
from server.utils.utils import Status


class AuthException(APIException):
    message = ''


class InvalidAPIKey(AuthException):
    message = 'Invalid API key'
    response_status = Status.BAD_REQUEST


class NoAPIKey(AuthException):
    message = 'No api key passed. API key is required'
    response_status = Status.BAD_REQUEST


class FailedToCreateAPIKey(AuthException):
    message = 'No api key passed. API key is required'
