from dataclasses import dataclass

from server.utils.base import ResponseModel


@dataclass
class Register(ResponseModel):
    key: str
