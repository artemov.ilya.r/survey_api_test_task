from server.utils.base import APIException
from server.utils.utils import Status


class TestServiceException(APIException):
    message = ''
    response_status = Status.SERVER_ERROR


class FailedToGetAllTests(TestServiceException):
    message = 'Failed to get all tests'
    response_status = Status.SERVER_ERROR


class FailedToGetQuestion(TestServiceException):
    message = 'Failed to get question'
    response_status = Status.SERVER_ERROR


class InvalidParams(TestServiceException):
    def __init__(self, key: str, reason: str, is_required: bool):
        super().__init__()
        self.message = f"Can't parse {key}: {reason}. Required field: {is_required}"

    response_status = Status.BAD_REQUEST
