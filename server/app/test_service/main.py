from collections import deque
from collections import deque
from typing import Any, Dict, List

from sqlalchemy import select

from server.app.test_service.exceptions import FailedToGetAllTests, InvalidParams
from server.app.test_service.response_models import QuestionResponse, TestsResponse
from server.app.test_service.validator import Validator
from server.db.init_db import db
from server.db.models import Test, Question, Option, Action, History, APIUser, HistoryEvent, Base
from server.utils.base import ResponseModel


class UserState:
    def __init__(self, api_key: str):
        self.db = db
        self.user = (
            self.db.session.query(APIUser)
                .filter(APIUser.key == api_key)
                .first()
        )

        self.current_test = None
        self.current_question = None

    def __caluclate_state(self):
        ...

    def add_answer(self, data: Dict[Any, Any]) -> None:
        questions = (
            self.db.session.query(Question)
                .filter(Question.test_id == data['test_id'])
                .all()
        )
        self.current_test = (
            self.db.session.query(Test)
                .filter(Test.test_id == data['test_id'])
                .first()
        )

        if len(questions) <= data['question_id']:
            raise InvalidParams(key='question_id', reason='Index out of range', is_required=True)

        self.current_question = questions[data['question_id']]

        data_to_store = []
        if self.current_question.is_text:
            data_to_store += self.__set_text_answer(data)
        elif self.current_question.is_match:
            data_to_store += self.__set_match_answer(data)
        else:
            data_to_store += self.__set_options_answer(data)

        self.db.store_sequences(data_to_store)

    def __set_text_answer(self, data: Dict[Any, Any]) -> List[Base]:
        if (
                'text' in data['answer'] and
                isinstance(data['answer']['text'], str)
        ):
            action = Action(
                text=data['answer']['text'],
                # test=self.current_question.test_id,
                # question=self.current_question.question_id,
            )
            # self.current_test.actions.append(action)
            history = History(
                # user=self.user.user_id,
                type=HistoryEvent.ANSWER,
                # action=action.action_id,
            )
            self.user.history_rows.append(history)
            action.history.append(history)
            return [action, history]
        else:
            raise InvalidParams(
                'answer.text',
                reason='this question requires text(\'str\') answer',
                is_required=True
            )

    def __set_options_answer(self, data: Dict[Any, Any]) -> List[Base]:
        data_to_store = []
        action = Action(
            # test=self.current_question.test_id,
            # question=self.current_question.question_id
        )
        self.current_test.actions.append(action)
        history = History(
            # user=self.user.user_id,
            type=HistoryEvent.ANSWER,
            # action=action.action_id
        )
        self.user.history_rows.append(history)
        action.history.append(history)
        data_to_store += [action, history, []]

        if 'options' in data['answer']:
            # for option_id in data['answer']['options']:
            # TODO: validate options
            action.options.append(
                self.db.session.query(Option)
                    .filter(Option.option_id.in_([data['answer']['options']]))
                    .all()
            )
                # data_to_store[2].append(ActionOptions(
                    # action_id=action.action_id,
                    # option=self.__get_validated_option_pk(option_id),
                # ))
        else:
            raise InvalidParams(
                'answer.options',
                reason='options required in this question type',
                is_required=True
            )
        return data_to_store

    def __set_match_answer(self, data: Dict[Any, Any]) -> List[Base]:
        data_to_store = []
        if 'categories' in data['answer']:
            question_categories = self.current_question.categories.split('|')
            action = Action(
                test=self.current_question.test_id,
                # question=self.current_question.question_id
            )
            self.current_test.actions.append(action)

            history = History(
                # user=self.user.user_id,
                type=HistoryEvent.ANSWER,
                # action=action.action_id
            )
            self.user.history_rows.append(history)
            action.history.append(history)
            data_to_store += [action, history, []]
            # TODO: validate
            for category_name, selected_options in data['answer']['categories'].items():
                try:
                    cat_id = question_categories.index(category_name)
                except Exception:
                    raise InvalidParams(
                        key='answer.categories',
                        reason=f'invalid category name: {category_name}',
                        is_required=True
                    )

                # TODO: validate
                # for option_id in selected_options:
                #     data_to_store[2].append(ActionOptions(
                #         # action_id=action.action_id,
                #         # option=self.__get_validated_option_pk(option_id),
                #         category=cat_id,
                #     ))
                action.options.append(
                    self.db.session.query(Option)
                        .filter(Option.option_id.in_(selected_options))
                        .all()
                )
        else:
            raise InvalidParams(
                'answer.categories',
                reason='categories required in this question type',
                is_required=True
            )
        return data_to_store

    def __get_validated_option_pk(self, option_id: int) -> int:
        # TODO: не делать каждый раз запрос, сделать один раз потом смотреть, возможно cached property в Question
        option_ids_from_db = (
            self.db.session.query(Option.option_id)
                .filter(Option.question_id == self.current_question.question_id)
                .all()
        )
        if option_id in (option[0] for option in option_ids_from_db):
            return option_id
        else:
            raise InvalidParams(
                key='answer.categories.option',
                reason=f'invalid {option_id=}',
                is_required=True
            )


class TestService:
    def __init__(self):
        self.db = db
        self.__cached_user_states = deque(maxlen=1000)

    def get_all_tests(self) -> ResponseModel:
        try:
            query = select(Test)
            db_result = self.db.session.execute(query).all()
            response = TestsResponse.create_from_db_model(db_result)
        except Exception as e:
            raise FailedToGetAllTests
        return response

    def get_question(self, data: Dict[Any, Any]) -> ResponseModel:
        data = Validator.get_and_validate('get_question', data)
        question = (self.db.session.query(Question)
                    .filter(Question.test_id == data['test_id'])
                    .all())

        if len(question) <= data['question_id']:
            raise InvalidParams(key='question_id', reason='Index out of range', is_required=True)

        question = question[data['question_id']]

        if question.is_text:
            return QuestionResponse.create_from_db_model(question)

        options = (self.db.session.query(Option)
                   .filter(Option.question_id == question.question_id)
                   .all())
        return QuestionResponse.create_from_db_model(question, options)

    def add_answer(self, data: Dict[Any, Any]) -> None:
        data = Validator.get_and_validate('add_answer', data)

        user_state = self.__get_user_state(api_key=data['api_key'])
        user_state.add_answer(data)

    def __get_user_state(self, api_key: str) -> UserState:
        try:
            return next(filter(lambda x: x[0] == api_key, self.__cached_user_states))
        except Exception:
            ...
        user_state = UserState(api_key)
        self.__cached_user_states.appendleft(user_state)
        return user_state


test_service = TestService()
