from typing import Dict, Any, Union

from server.app.test_service.exceptions import InvalidParams


class Validator:
    @staticmethod
    def get_and_validate(method: str, data: Dict[Any, Any]):
        cleaned_data = {}
        if method == 'get_question':
            cleaned_data['test_id'] = Validator.__get_int(data, 'test_id', is_required=True)
            cleaned_data['question_id'] = Validator.__get_int(data, 'question_id', is_required=True)
        if method == 'add_answer':
            cleaned_data['test_id'] = Validator.__get_int(data, 'test_id', is_required=True)
            cleaned_data['question_id'] = Validator.__get_int(data, 'question_id', is_required=True)
            cleaned_data['answer'] = Validator.__get_dict(data, 'answer', is_required=True)
            cleaned_data['api_key'] = Validator.__get_str(data, 'api_key', is_required=True)

        return cleaned_data

    @staticmethod
    def __get_str(data: Dict[str, Any], key: str, is_required: bool = False) -> int:
        var = data.get(key)
        if not var:
            raise InvalidParams(key, 'is None', is_required)
        return var

    @staticmethod
    def __get_int(data: Dict[str, Any], key: str, is_required: bool = False) -> int:
        var = data.get(key)
        if var is None:
            raise InvalidParams(key, 'is None', is_required)
        try:
            return int(var)
        except Exception as e:
            raise InvalidParams(key, f'cant parse. {e}', is_required)

    @staticmethod
    def __get_list(data: Dict[str, Any], key: str, is_required: bool = False) -> list:
        var = data.get(key)
        if not var:
            raise InvalidParams(key, 'is None', is_required)

        if not isinstance(var, list):
            raise InvalidParams(key, f'is not list', is_required)

        return var

    @staticmethod
    def __get_dict(data: Dict[str, Any], key: str, is_required: bool = False) -> dict:
        var = data.get(key)
        if not var:
            raise InvalidParams(key, 'is None', is_required)

        if not isinstance(var, dict):
            raise InvalidParams(key, f'is not dict', is_required)

        return var