from dataclasses import dataclass
from typing import List, Optional

from sqlalchemy.engine import Row

from server.db.models import Question, Option
from server.utils.base import ResponseModel


@dataclass
class TestsResponse(ResponseModel):
    tests: List['Test']

    @staticmethod
    def create_from_db_model(data: List[Row]) -> 'TestsResponse':
        return TestsResponse(
            tests=[
                Test(
                    id=row[0].test_id,
                    name=row[0].name,
                    is_available=False,
                    is_passed=False,
                ) for row in data
            ]
        )


@dataclass
class Test:
    id: int
    name: str
    is_available: bool
    is_passed: bool


@dataclass
class QuestionResponse(ResponseModel):
    text: str
    options: Optional['QuestionOption']

    is_multiple: Optional[bool]
    is_match: Optional[bool]
    is_text: Optional[bool]

    categories: Optional[List[str]]

    @staticmethod
    def create_from_db_model(question: Question, options: Optional[Option] = None) -> 'QuestionResponse':
        return QuestionResponse(
            text=question.text,
            is_multiple=question.is_multiple,
            is_match=question.is_match,
            is_text=question.is_text,
            categories=question.categories.split('|') if question.categories else None,
            options=[
                QuestionOption(
                    text=option.text,
                    id=option.option_id,
                ) for option in options
            ] if options else None
        )


@dataclass
class QuestionOption:
    text: str
    id: int

