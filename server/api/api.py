from aiohttp import web

from server.app.auth.main import auth_service
from server.app.auth.response_models import Register
from server.app.test_service.main import test_service
from server.db.init_db import db
from server.utils.decorators import auth_required, error_response


class Server:
    def __init__(self):
        self.db = db
        self.auth_service = auth_service
        self.test_service = test_service

        app = web.Application()
        app.add_routes(
            [
                web.get('/register_key', self.register),
                web.get('/get_all_tests', self.get_all_tests),
                web.get('/get_question', self.get_question),

                web.post('/add_answer', self.add_answer),

                web.get('/check_auth', self.check_auth),
            ]
        )
        web.run_app(app)

    async def register(self, request: web.Request) -> web.Response:
        key = self.auth_service.register_new()
        response = Register(key=key)
        return response.to_json_response()

    @error_response
    @auth_required
    async def check_auth(self, request: web.Request) -> web.Response:
        return web.json_response({'result': 'ok'})

    @error_response
    @auth_required
    async def get_all_tests(self, request: web.Request) -> web.Response:
        result = self.test_service.get_all_tests()
        return result.to_json_response()

    @error_response
    @auth_required
    async def get_question(self, request: web.Request) -> web.Response:
        result = self.test_service.get_question({
                'question_id': request.query.get('question_id'),
                'test_id': request.query.get('test_id'),
            })
        return result.to_json_response()

    @error_response
    @auth_required
    async def add_answer(self, request: web.Request) -> web.Response:
        body = await request.json()
        body['api_key'] = request.query.get('api_key')
        self.test_service.add_answer(body)
        return web.Response(status=web.HTTPOk.status_code)


if __name__ == '__main__':
    server = Server()
