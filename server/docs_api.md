---
> Зарегистрировать новый API ключ
>
>path: /register_key
>
>type: GET

response:

 ```json
{
  "key": "641a21cb469643f798004ba8546e1034"
}
```

---
> **ТЕСТОВЫЙ**
>
> Проверить API ключ
>
>path: ```/check_auth?api_key=<api_key:str>```
>
>type: GET
---

>
> Список доступных тестов
>
>path: ```/get_all_tests```
>
>type: GET response:

```json
{
  "tests": [
    {
      "id": <id
      :
      int>,
      "name": <name
      :
      str>,
      "is_available": <is_available
      :
      bool>,
      "is_passed": <is_passed
      :
      bool>
    },
    ...
  ]
}
```

---

>
> Получить вопрос из теста
>
>path: ```/get_question?test_id=<id:int>&question_id=<id:int>```
>
>type: GET

response:

```json
{
  "text": str,
  "options": [
    {
      "text": str,
      "id": int
    },
    ...
  ],
  "is_multiple": bool,
  "is_match": bool,
  "is_text": bool,
  "categories": [
    str,
    str,
    ...
  ]
}
```

---