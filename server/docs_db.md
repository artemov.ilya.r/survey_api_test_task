Schema:
...

## History Types


| Event type  | Description      |
|-------------|------------------|
| START  | The user started the test|
| ANSWER | The user gave an answer to the test      |
| END    | The user has finished the test  |