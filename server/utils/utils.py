import logging

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter(
    '[%(levelname)s] %(asctime)s: [%(filename)s.%(funcName)s] %(message)s'
)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class Status:
    OK = 200
    BAD_REQUEST = 400
    SERVER_ERROR = 500
