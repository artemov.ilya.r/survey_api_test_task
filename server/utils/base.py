import dataclasses
from dataclasses import dataclass

from aiohttp import web

from server.utils.utils import Status


@dataclass
class ResponseModel:
    def to_json_response(self) -> web.Response:
        return web.json_response(dataclasses.asdict(self))


class APIException(Exception):
    message = ''
    response_status = Status.SERVER_ERROR
