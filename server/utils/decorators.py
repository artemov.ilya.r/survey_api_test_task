from copy import copy
from functools import partial

from aiohttp import web

from server.app.auth.exceptions import InvalidAPIKey, NoAPIKey
from server.app.auth.main import auth_service
from server.utils.base import APIException
from server.utils.utils import logger, Status


class error_response:
    def __init__(self, func=None):
        if func is not None:
            self.func = func
        else:
            raise RuntimeError

    async def __call__(self, instance, *args):
        try:
            ret = await self.func(instance, *args)
        except APIException as e:
            logger.error(e.message)
            return web.json_response(
                {'error': e.message},
                status=e.response_status
            )
        except Exception as e:
            logger.exception(e)
            return web.json_response(
                {'error': 'Internal server error'},
                status=Status.SERVER_ERROR
            )
        else:
            return ret

    def __get__(self, instance, owner):
        return partial(self, instance)


class auth_required:
    auth_service = auth_service

    def __init__(self, func=None):
        if func is not None:
            self.func = func
        else:
            raise RuntimeError

    def __validate(self, request: web.Request) -> None:
        key = request.query.get('api_key')
        self.auth_service.validate_key(key)

    async def __call__(self, instance, *args):
        if len(args) < 1:
            raise RuntimeError
        if not isinstance(args[0], web.Request):
            raise RuntimeError

        self.__validate(args[0])

        ret = await self.func(instance, *args)

        return ret

    def __get__(self, instance, owner):
        return partial(self, instance)
