from enum import Enum, unique

from sqlalchemy import Table, Integer, Column, String, ForeignKey, PrimaryKeyConstraint, Boolean, DateTime
from sqlalchemy.dialects.postgresql import ENUM as pgEnum
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.sql import func

Base = declarative_base()


class APIUser(Base):
    __tablename__ = 'apiuser'

    user_id = Column(Integer(), primary_key=True, autoincrement=True, unique=True)

    key = Column(String(32), primary_key=True)

    __table_args__ = (
        PrimaryKeyConstraint(key),
        PrimaryKeyConstraint(user_id),
    )


class Test(Base):
    __tablename__ = 'test'

    test_id = Column(Integer(), primary_key=True, autoincrement=True)

    name = Column(String(2000))
    description = Column(String(2000), nullable=True)
    number = Column(Integer())
    study_data = Column(String(2000), nullable=True)

    __table_args__ = (
        PrimaryKeyConstraint(test_id),
    )


class Question(Base):
    __tablename__ = 'question'

    question_id = Column(Integer(), primary_key=True, autoincrement=True)

    text = Column(String(2000))

    is_multiple = Column(Boolean(), default=False)
    is_option = Column(Boolean(), default=False)
    is_match = Column(Boolean(), default=False)
    is_text = Column(Boolean(), default=False)

    right_text_pattern = Column(String(2000), nullable=True, default=None)

    categories = Column(String(2000), nullable=True, default=None)

    test_id = Column(Integer, ForeignKey(Test.test_id))
    test = relationship('Test', backref='questions')

    __table_args__ = (
        PrimaryKeyConstraint(question_id),
    )


class Option(Base):
    __tablename__ = 'option'

    option_id = Column(Integer(), primary_key=True, autoincrement=True)

    text = Column(String(2000))

    is_right = Column(Boolean(), nullable=True, default=None)
    right_category = Column(Integer, nullable=True, default=None)

    question_id = Column(Integer, ForeignKey(Question.question_id))
    question = relationship('Question', backref='options')

    __table_args__ = (
        PrimaryKeyConstraint(option_id),
    )


# class ActionOptions(Base):
#     action_id = Column(ForeignKey('action.action_id'), primary_key=True)
#     action =
#     option_id = Column(ForeignKey('option.option_id'), primary_key=True)
#     options = relationship("Option", secondary='actions_options')

action_options = Table(
    'actions_options',
    Base.metadata,
    Column('action_id', ForeignKey('action.action_id'), primary_key=True),
    Column('option_id', ForeignKey('option.option_id'), primary_key=True),
    Column('category', Integer())
)


class Action(Base):
    __tablename__ = 'action'

    action_id = Column(Integer(), primary_key=True, autoincrement=True)

    text = Column(String(2000), nullable=True)

    options = relationship("Option", secondary='actions_options')
    # test_id = Column(ForeignKey(Test.test_id), nullable=True)
    # test = relationship('Test', backref='actions')
    #
    # question_id = Column(ForeignKey(Question.question_id), nullable=True)
    # question = relationship('Question', backref='actions')

    __table_args__ = (
        PrimaryKeyConstraint(action_id),
    )


# class ActionOptions(Base):
#     __tablename__ = 'actionoptions'
#
#     action_option_id = Column(Integer(), primary_key=True, autoincrement=True)
#
#     category = Column(Integer())
#
#     action = relationship('Action', backref='options')
#     action_id = Column(ForeignKey(Action.action_id))
#
#     option_id = Column(ForeignKey(Option.option_id))
#     option = relationship('Option', backref='actions')
#
#     __table_args__ = (
#         PrimaryKeyConstraint(action_option_id),
#     )


@unique
class HistoryEvent(Enum):
    START = 'START'
    ANSWER = 'ANSWER'
    END = 'END'


class History(Base):
    __tablename__ = 'history'

    history_id = Column(Integer(), primary_key=True, autoincrement=True)

    dt = Column(DateTime(), server_default=func.now())

    user_id = Column(ForeignKey(APIUser.user_id))
    user = relationship('APIUser', backref='history_rows')

    type = Column(pgEnum(HistoryEvent), unique=False, nullable=False)

    action_id = Column(ForeignKey(Action.action_id), nullable=True)
    action = relationship('Action', backref='history')

    __table_args__ = (
        PrimaryKeyConstraint(history_id),
    )
