import os
from typing import Union, List

from sqlalchemy import (
    create_engine
)

from sqlalchemy.orm import Session
from sqlalchemy_utils import create_database, drop_database
from sqlalchemy_utils.functions import database_exists

from server.app.test_service.exceptions import InvalidParams
from server.db.models import Base, APIUser, Test, Question, Option, action_options, Action, History, HistoryEvent
from server.utils.utils import logger


class Database:
    def __init__(self):
        self.db_user = os.getenv('POSTGRES_USER')
        self.db_password = os.getenv('POSTGRES_PASSWORD')
        self.db_ip = os.getenv('POSTGRES_IP')
        self.db_name = os.getenv('POSTGRES_DB')
        self.db_port = os.getenv('DB_PORT')

        # formatting
        self.db_host = os.getenv("PG_HOSTNAME") if os.getenv("PG_HOSTNAME") else "localhost"
        logger.warning('Start connecting to database with config')
        logger.warning(f"{os.getenv('POSTGRES_USER')=}")
        logger.warning(f"{os.getenv('POSTGRES_PASSWORD')=}")
        logger.warning(f"{os.getenv('POSTGRES_IP')=}")
        logger.warning(f"{os.getenv('POSTGRES_DB')=}")
        logger.warning(f"{os.getenv('DB_PORT')=}")

        # import time
        # time.sleep(5)

        self.engine = create_engine(
            f'postgresql+psycopg2://{self.db_user}:{self.db_password}@{self.db_host}:{self.db_port}/{self.db_name}',
            echo=False,
        )
        # Uncomment if need
        # [WARNING] THIS CODE DROP ALL DB DATA
        drop_database(self.engine.url)
        if not database_exists(self.engine.url):
            create_database(self.engine.url)

        Base.metadata.create_all(self.engine, checkfirst=True)
        self.session = Session(bind=self.engine)

        self.__add_test_data()

    def __add_test_data(self):
        self.test_u = APIUser(
            key='API_KEY_FOR_TESTING_dcd3157b6aa0'
        )
        self.store(self.test_u)
        self.create_test_for_test_2(0)
        self.create_test_for_test_2(1)
        self.__add_test_run()

    def __add_test_run(self):
        def add_text_answer():
            data = {
                "test_id": 1,
                "question_id": 1,
                "answer": {
                    "text": "4"
                }
            }
            questions = (
                self.session.query(Question)
                    .filter(Question.test_id == data['test_id'])
                    .all()
            )
            self.current_test = (
                self.session.query(Test)
                    .filter(Test.test_id == data['test_id'])
                    .first()
            )
            action = Action(text=data['answer']['text'])
            history = History(type=HistoryEvent.ANSWER)
            self.test_u.history_rows.append(history)
            action.history.append(history)
            self.store([history, action])
        def add_option_answer():

            data = {
                "test_id": 1,
                "question_id": 3,
                "answer": {
                    "options": [5, 6]
                }
            }
            questions = (
                self.session.query(Question)
                    .filter(Question.test_id == data['test_id'])
                    .all()
            )
            self.current_test = (
                self.session.query(Test)
                    .filter(Test.test_id == data['test_id'])
                    .first()
            )
            data_to_store = []
            action = Action()

            history = History(type=HistoryEvent.ANSWER)
            self.test_u.history_rows.append(history)
            action.history.append(history)
            data_to_store += [action, history]

            action.options.extend(
                self.session.query(Option)
                    .filter(Option.option_id.in_(
                        data['answer']['options']
                    ))
                    .all()
            )
            self.store(data_to_store)

        add_text_answer()
        add_option_answer()

    # TODO: move to auth
    def get_user(self, key: str) -> APIUser:
        result = self.session.query(
            APIUser
        ).filter(APIUser.key == key).first()
        return result

    def store(self, data: Union[Base, List[Base]]) -> None:
        if isinstance(data, Base):
            self.__store_one(data)
        elif isinstance(data, list):
            self.__store_many(data)

    def store_sequences(self, data: List[List[Base]]) -> None:
        for sequence in data:
            self.store(sequence)

    def __store_one(self, obj: Base) -> None:
        self.session.add(obj)
        self.session.commit()

    def __store_many(self, data: List[Base]) -> None:
        self.session.add_all(data)
        # for obj in data:
            # self.session.add(obj)
        self.session.commit()

    def create_test_for_test_2(self, i: int) -> None:
        data = []
        test = Test(
            name=f'Test for test #{i}:)',
            number=i
        )
        data.append(test)

        question_text = Question(
            text='2+2=?',
            is_text=True,
            right_text_pattern='4',
        )
        test.questions.append(question_text)

        question_one_option = Question(
            text='2+2=?',
            is_option=True,
        )
        test.questions.append(question_one_option)
        data.append(question_one_option)

        options = [
            Option(
                text=str(i),
                is_right=False if i != 4 else True,
            ) for i in range(1, 5)
        ]
        for opt in options:
            question_one_option.options.append(opt)

        question_two_option = Question(
            text='select first two',
            is_multiple=True,
        )
        test.questions.append(question_two_option)

        options = [
            Option(
                text=str(i),
                is_right=True if i in (1, 2) else False,
            ) for i in range(1, 5)
        ]
        for opt in options:
            question_two_option.options.append(opt)

        question_match = Question(
            text='do match',
            is_match=True,
            categories='<=3|>3',
        )
        test.questions.append(question_match)
        data.append(question_match)

        options = [
            Option(
                text=str(i),
                right_category=0 if i <= 3 else 1,
            ) for i in range(1, 5)
        ]
        for opt in options:
            question_match.options.append(opt)
        self.store(test)


db = Database()
