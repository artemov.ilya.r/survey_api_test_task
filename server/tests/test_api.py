import json
import uuid

import pytest
import requests


class TestApi:
    url = 'http://0.0.0.0:8080'
    valid_key = 'API_KEY_FOR_TESTING_dcd3157b6aa0'

    def __build_url(self, method, api_key=None):
        url = f'{self.url}/{method}'
        if api_key:
            url += f'?api_key={api_key}'
        return url

    def test_register_key(self):
        result = requests.get(self.__build_url('register_key'))
        key = json.loads(result.content)['key']
        assert isinstance(key, str)

    def test_check_auth(self):
        url_no_key = self.__build_url('check_auth')
        url_invalid_key = self.__build_url('check_auth', uuid.uuid4().hex)
        url_valid_key = self.__build_url('check_auth', self.valid_key)

        no_key_reposnse = requests.get(url_no_key)
        invalid_key_reposnse = requests.get(url_invalid_key)
        valid_key_reposnse = requests.get(url_valid_key)

        assert no_key_reposnse.status_code == 400
        assert invalid_key_reposnse.status_code == 400
        assert valid_key_reposnse.status_code == 200

    def test_get_all_tests(self):
        ...

    def test_get_question(self):
        ...

    def test_add_answer(self):
        post_data = [
            {
                "test_id": 1,
                "question_id": 0,
                "answer": {
                    "text": "4"
                }
            },
            {
                "test_id": 1,
                "question_id": 1,
                "answer": {
                    "options": [4]
                }
            },
            {
                "test_id": 1,
                "question_id": 2,
                "answer": {
                    "options": [5, 6]
                }
            },
            {
                "test_id": 1,
                "question_id": 3,
                "answer": {
                    "categories": {
                        "<=3": [9, 10, 11],
                        ">3": [12]
                    }
                }
            }
        ]
        invalid_post_data = [
            {
                "test_id": 1,
                "question_id": 1,
                "answer": {
                    "text": "4"
                }
            },
            {
                "test_id": 1,
                "question_id": 2,
                "answer": {
                    "options": [4]
                }
            },
            {
                "test_id": 1,
                "question_id": 3,
                "answer": {
                    "options": [5, 6]
                }
            },
            {
                "test_id": 1,
                "question_id": 4,
                "answer": {
                    "categories": {
                        "<=3": [9, 10, 11],
                        ">3": [12]
                    }
                }
            }
        ]
        for data in post_data:
            response = requests.post(
                self.__build_url('add_answer', api_key=self.valid_key),
                data=json.dumps(data)
            )
            assert response.status_code == 200

        for data in invalid_post_data:
            response = requests.post(
                self.__build_url('add_answer', api_key=self.valid_key),
                data=json.dumps(data)
            )
            assert response.status_code in [400, 500]
